//
// Copyright (c) 2022 Statehub Ltd. All rights reserved.
// Use is subject to license terms.
//

use std::fmt;
use std::ops;
use std::str;

use schemars::schema::InstanceType;
use schemars::schema::SingleOrVec;
use serde_with::{DeserializeFromStr, SerializeDisplay};

/// a general `id` for state-hub resources
#[derive(
    Clone, Copy, DeserializeFromStr, SerializeDisplay, PartialEq, Eq, PartialOrd, Ord, Hash,
)]
pub struct StatehubId([u8; Self::LEN]);
impl StatehubId {
    pub const ALPHABET: &'static [char] = nanoid_dictionary::ALPHANUMERIC_LOWERCASE;

    pub const LEN: usize = 21;
    const IDENT: &'static str = "StatehubId";
}

impl StatehubId {
    pub fn new() -> Self {
        let mut this = Self::default();
        let len = this.0.len();
        this.0
            .copy_from_slice(nanoid::nanoid!(len, Self::ALPHABET).as_bytes());
        this
    }

    pub fn as_str(&self) -> std::borrow::Cow<'_, str> {
        String::from_utf8_lossy(&self.0)
    }
}

impl Default for StatehubId {
    fn default() -> Self {
        Self(*b"000000000000000000000")
    }
}

impl ops::Deref for StatehubId {
    type Target = [u8; Self::LEN];
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl str::FromStr for StatehubId {
    type Err = anyhow::Error;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        anyhow::ensure!(
            s.len() == Self::LEN,
            "{} length != {}::len, ({} != {})",
            s,
            Self::IDENT,
            s.len(),
            Self::LEN,
        );

        anyhow::ensure!(
            s.chars().all(|ch| Self::ALPHABET.contains(&ch)),
            "{} has chars not in {:?}",
            s,
            Self::ALPHABET
        );

        let mut this = Self::default();
        this.0.copy_from_slice(s.as_bytes());
        Ok(this)
    }
}

impl fmt::Display for StatehubId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(self.as_str().as_ref())
    }
}

impl fmt::Debug for StatehubId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        <str as fmt::Debug>::fmt(self.as_str().as_ref(), f)
    }
}

impl schemars::JsonSchema for StatehubId {
    fn is_referenceable() -> bool {
        false
    }

    fn schema_name() -> String {
        Self::IDENT.to_string()
    }

    fn json_schema(_gen: &mut schemars::gen::SchemaGenerator) -> schemars::schema::Schema {
        schemars::schema::Schema::Object(schemars::schema::SchemaObject {
            string: Some(Box::new(schemars::schema::StringValidation {
                max_length: Self::LEN.try_into().ok(),
                min_length: Self::LEN.try_into().ok(),
                pattern: Some(format!("[a-z0-9]{{{}}}", Self::LEN)),
            })),
            instance_type: Some(SingleOrVec::Single(Box::new(InstanceType::String))),
            ..schemars::schema::SchemaObject::default()
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn default_statehub_id() {
        let id = StatehubId::default();
        let id: StatehubId = id.to_string().parse().unwrap();
        assert_eq!(id, StatehubId::default())
    }

    #[test]
    fn schema_rs_print() {
        let x = schemars::schema_for!(StatehubId);
        println!("{}", serde_json::to_string_pretty(&x).unwrap());
    }
}
