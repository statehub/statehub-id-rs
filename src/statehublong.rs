//
// Copyright (c) 2022 Statehub Ltd. All rights reserved.
// Use is subject to license terms.
//

use std::fmt;
use std::fmt::Formatter;
use std::str;

use schemars::schema::InstanceType;
use schemars::schema::SingleOrVec;
use serde_with::{DeserializeFromStr, SerializeDisplay};

/// A wrapper around u64 what is `serialized` into `string`
/// preventing erroneous behaviour around large number for `json` serialization
#[derive(
    Clone,
    Copy,
    Debug,
    Default,
    DeserializeFromStr,
    SerializeDisplay,
    PartialEq,
    Eq,
    PartialOrd,
    Ord,
    Hash,
)]
pub struct StatehubLong(u64);
impl StatehubLong {
    /// (2 `power-of` 64) - 1 = 18_446_744_073_709_551_615 - 20-chars
    const MAX_LEN: u32 = 20;
    const IDENT: &'static str = "StatehubLong";
}

impl From<u64> for StatehubLong {
    fn from(n: u64) -> Self {
        Self(n)
    }
}

impl From<usize> for StatehubLong {
    fn from(n: usize) -> Self {
        Self(n as u64)
    }
}

impl From<StatehubLong> for u64 {
    fn from(long: StatehubLong) -> Self {
        long.0
    }
}

impl std::ops::Deref for StatehubLong {
    type Target = u64;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl std::fmt::Display for StatehubLong {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl str::FromStr for StatehubLong {
    type Err = <u64 as str::FromStr>::Err;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        s.parse().map(Self)
    }
}

impl schemars::JsonSchema for StatehubLong {
    fn is_referenceable() -> bool {
        false
    }

    fn schema_name() -> String {
        Self::IDENT.to_string()
    }

    fn json_schema(_gen: &mut schemars::gen::SchemaGenerator) -> schemars::schema::Schema {
        schemars::schema::Schema::Object(schemars::schema::SchemaObject {
            string: Some(Box::new(schemars::schema::StringValidation {
                max_length: Some(Self::MAX_LEN),
                min_length: Some(1),
                pattern: Some("[0-9]".into()),
            })),
            instance_type: Some(SingleOrVec::Single(Box::new(InstanceType::String))),
            ..schemars::schema::SchemaObject::default()
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_long() {
        for (n, len) in [(0, 1), (u64::MAX, StatehubLong::MAX_LEN as usize)] {
            let l: StatehubLong = n.into();
            let ser = serde_json::to_string(&l).unwrap();
            // expect 0 to be wrapped by `""`
            assert_eq!(ser.len(), len + 2);
            let de = serde_json::from_str(&ser).unwrap();
            assert_eq!(l, de);
        }
    }
}
